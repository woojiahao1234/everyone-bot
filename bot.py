import discord
from secret import Token

client = discord.Client()
token = Token()


async def spam_user(message):
	await client.delete_message(message)
	members_count = message.server.member_count
	for i in range(members_count):
		ping = await client.send_message(message.channel, content = message.author.mention)
		await client.delete_message(ping)


@client.event
async def on_message(message):
	if '@everyone' in message.content and not message.author.bot:
		await spam_user(message)


client.run(token.get_token())
